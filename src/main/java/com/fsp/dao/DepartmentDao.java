package com.fsp.dao;

import com.fsp.pojo.Department;

import java.util.List;

public interface DepartmentDao {
    int deleteByPrimaryKey(Integer deptId);

    int insert(Department record);

    int insertSelective(Department record);

    /**
     * 根据部门id进行查询部门数据
     * @param deptId
     * @return
     */
    Department selectByPrimaryKey(Integer deptId);

    int updateByPrimaryKeySelective(Department record);

    int updateByPrimaryKey(Department record);

    /**
     * 根据build条件查询部门
     * @param deptName
     * @return
     */
    List<Department> selectByDeptName(String deptName);
}