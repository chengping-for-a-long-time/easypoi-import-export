package com.fsp.pojo;

import java.io.Serializable;

import cn.afterturn.easypoi.excel.annotation.Excel;
import cn.afterturn.easypoi.excel.annotation.ExcelTarget;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * department
 * @author 
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
@ExcelTarget("department")
public class Department implements Serializable {
    private static final long serialVersionUID = 1L;
    /**
     * 部门ID
     */
    private Integer deptId;

    /**
     * 部门名
     */
    @Excel(name = "部门")
    private String deptName;

    /**
     * 地址
     */
    private String address;
}