package com.fsp.pojo;

import java.io.Serializable;
import java.util.Date;

import cn.afterturn.easypoi.excel.annotation.Excel;
import cn.afterturn.easypoi.excel.annotation.ExcelEntity;
import cn.afterturn.easypoi.excel.annotation.ExcelIgnore;
import cn.afterturn.easypoi.excel.annotation.ExcelTarget;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * user
 * @author 
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
@ExcelTarget("user")
public class User implements Serializable {
    private static final long serialVersionUID = 1L;
    /**
     * 用户ID
     */
    @Excel(name = "ID",suffix = "号")
    private Integer userId;

    /**
     * 用户名
     */
    @Excel(name = "姓名")
    private String userName;

    /**
     * 工龄
     */
    @Excel(name = "工龄",suffix = "年")
    private Integer workAge;

    /**
     * 性别
     */
    @Excel(name = "性别",replace = {"男_1","女_0"})
    private Integer sex;

    /**
     * 生日
     */
    @Excel(name = "生日",format = "yyyy年MM月dd日",width = 20)
    private Date birthday;

    /**
     * 部门ID
     */
    @ExcelIgnore
    private Integer deptId;

    /**
     * 所属部门
     */
    @ExcelEntity
    private Department department;
}