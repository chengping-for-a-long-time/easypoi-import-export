package com.fsp.service;

import com.fsp.pojo.Department;

import java.util.List;

/**
 * @author fushengping
 * @interface DepartmentService
 * @description
 * @date 2021/12/6 14:08
 */
public interface DepartmentService {
    /**
     * 根据用户所属部门id,查询该部门
     * @param deptId
     * @return
     */
    Department getById(Integer deptId);

    /**
     * 条件查询部门
     * @param deptName
     * @return
     */
    List<Department> listDepartments(String deptName);

}
