package com.fsp.service;

import com.fsp.pojo.User;
import com.fsp.vo.PageParam;
import com.github.pagehelper.PageInfo;

import java.util.List;

/**
 * @author fushengping
 * @interface UserService
 * @description
 * @date 2021/12/6 14:08
 */
public interface UserService {
    /**
     * 获取全部用户
     * @return
     */
    List<User> listUsers();

    /**
     * 插入用户
     * @param user
     */
    void insert(User user);

    /**
     * 查询分页
     * @param pageParam
     * @return
     */
    PageInfo<User> queryPageInfo(PageParam pageParam);
}
