package com.fsp.service.impl;

import com.fsp.dao.DepartmentDao;
import com.fsp.pojo.Department;
import com.fsp.service.DepartmentService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

/**
 * @author fushengping
 * @className DepartmentServiceImpl
 * @description
 * @date 2021/12/6 14:09
 */
@Service
public class DepartmentServiceImpl implements DepartmentService {

    @Resource
    private DepartmentDao departmentDao;
    @Override
    public Department getById(Integer deptId) {
        return departmentDao.selectByPrimaryKey(deptId);
    }

    @Override
    public List<Department> listDepartments(String deptName) {
        return departmentDao.selectByDeptName(deptName);
    }

}
