package com.fsp.service.impl;

import com.fsp.dao.UserDao;
import com.fsp.pojo.User;
import com.fsp.service.UserService;
import com.fsp.vo.PageParam;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import lombok.AllArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

/**
 * @author fushengping
 * @className UserServiceImpl
 * @description
 * @date 2021/12/6 14:10
 */
@Service
public class UserServiceImpl implements UserService {
    @Resource
    private UserDao userDao;
    @Override
    public List<User> listUsers() {
        return userDao.selectAllUser();
    }

    @Override
    public void insert(User user) {
        userDao.insert(user);
    }

    @Override
    public PageInfo<User> queryPageInfo(PageParam pageParam) {
        //传入当前页和每页面展示个数
        PageHelper.startPage(pageParam.getCurrentPage(), pageParam.getPageSize());
        List<User> userList = userDao.selectAllUser();
        return new PageInfo<>(userList);
    }
}
