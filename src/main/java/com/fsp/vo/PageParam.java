package com.fsp.vo;

import lombok.Data;

/**
 * @author fushengping
 * @className PageParam
 * @description
 * @date 2021/12/6 16:20
 */
@Data
public class PageParam {
    /**
     * 当前页
     */
    private Integer currentPage;
    /**
     * 每页数量
     */
    private Integer pageSize;

    /**
     * 其他查询条件（用户名查询）
     */
//    private String queryString;
}
